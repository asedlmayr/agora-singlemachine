"""This profile instantiates a d840/d740 machine running Ubuntu 20.04 64-bit LTS. The machine can be connected to a Skylark FAROS massive MIMO system comprised of a FAROS hub, a Faros massive MIMO Base Station, and a set of Iris UEs (clients).  Additionally, the image has Intel MKL libraries, Flexran, and all other compilation dependencies of Agora preinstalled.

The PC includes a MATLAB installation that could be used to run experiments on FAROS with RENEWLab demos. For more information on RENEWLab, see [RENEW documentation page](https://wiki.renew-wireless.org/)


Instructions:

- For more information on Agora please refer to our [github](https://github.com/jianding17/Agora/wiki) and [wiki](https://github.com/jianding17/Agora) sites.  

"""

import geni.portal as portal
import geni.urn as urn
import geni.rspec.pg as pg
import geni.rspec.emulab as elab
import geni.rspec.emulab.spectrum as spectrum

# Resource strings
#PCIMG = 'urn:publicid:IDN+emulab.net+image+argos-test:agora-ubuntu-2004'
PCIMG = 'urn:publicid:IDN+emulab.net+image+argos-test:agora-2004-base'
INTEL_LIBS_URN = 'urn:publicid:IDN+emulab.net:argos-test+imdataset+oneapi-flexran21'
MATLAB_DS_URN = 'urn:publicid:IDN+emulab.net:powdersandbox+imdataset+matlab2021ra-etc'
MATLAB_MP = "/usr/local/MATLAB"
STARTUP_SCRIPT = "/local/repository/faros_start.sh"
#PCHWTYPE = "d840"
PCHWTYPE = "d740"
FAROSHWTYPE = "faros_sfp"
IRISHWTYPE = "iris030"

REMDS_TYPES = [("readonly", "Read Only"),
               ("rwclone", "Read-Write Clone (not persistent)"),
               ("readwrite", "Read-Write (persistent)")]

MMIMO_ARRAYS = ["", ("mmimo1-honors", "Honors rooftop array"),
                ("mmimo1-meb", "Meb basestation array")]

UE = ["", ("irisclients2-meb", "Meb UEs")]

#
# Profile parameters.
#
pc = portal.Context()

# Frequency/spectrum parameters
pc.defineStructParameter(
    "freq_ranges", "Range", [],
    multiValue=True,
    min=1,
    multiValueTitle="Frequency ranges for over-the-air operation.",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Min",
            portal.ParameterType.BANDWIDTH,
            3540.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Max",
            portal.ParameterType.BANDWIDTH,
            3550.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ])

# Array to allocate
pc.defineStructParameter(
    "mmimo_devices", "mMIMO Devices", [],
    multiValue=True,
    min=0,
    multiValueTitle="Massive MIMO basestations to allocate.",
    members=[
        portal.Parameter(
            "mmimoid", "ID of Massive MIMO array to allocate.",
            portal.ParameterType.STRING, MMIMO_ARRAYS[0], MMIMO_ARRAYS
        ),
    ])

pc.defineStructParameter(
    "ue_devices", "UE Devices", [],
    multiValue=True,
    min=0,
    multiValueTitle="Iris UE clients to allocate.",
    members=[
        portal.Parameter(
            "ueid", "ID of Iris UE to allocate.",
            portal.ParameterType.STRING, UE[0], UE
        ),
    ])

pc.defineParameter("matlabds", "Attach the Matlab dataset to the compute host.",
                   portal.ParameterType.BOOLEAN, True)

pc.defineParameter("intellibs", "Attach the intel and 3rd party library dataset to the compute host.",
                   portal.ParameterType.BOOLEAN, True)

pc.defineParameter("intelmountpt", "Mountpoint for 3rd party libraries and inteloneAPI",
                   portal.ParameterType.STRING, "/opt")

pc.defineParameter("INTEL_LIBS_URN", "URN of the 3rd party library dataset", 
                   portal.ParameterType.STRING,
                   INTEL_LIBS_URN)

pc.defineParameter("hubints", "Number of interfaces to attach on hub (def: 2)",
                   portal.ParameterType.INTEGER, 2,
                   longDescription="This can be a number between 1 and 4.")

pc.defineParameter("fixedid", "Fixed PC Node_id (Optional)",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Fix 'pc1' to this specific node.  Leave blank to allow for any available node of the correct type.")

# Bind and verify parameters.
params = pc.bindParameters()

for i, frange in enumerate(params.freq_ranges):
    if frange.freq_max - frange.freq_min < 1:
        perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ["freq_ranges[%d].freq_min" % i, "freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)

if params.hubints < 1 or params.hubints > 4:
    perr = portal.ParameterError("Number of interfaces on hub to connect must be between 1 and 4 (inclusive).")
    portal.context.reportError(perr)

pc.verifyParameters()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Mount a remote dataset
def connect_DS(node, urn, mp, dsname = "", dstype = "rwclone"):
    if not dsname:
        dsname = "ds-%s" % node.name
    bs = request.Blockstore(dsname, mp)
    if dstype == "rwclone":
        bs.rwclone = True
    elif dstype == "readonly":
        bs.readonly = True
        
    # Set dataset URN
    bs.dataset = urn

    # Create link from node to OAI dataset rw clone
    bslink = request.Link("link_%s" % dsname, members=(node, bs.interface))
    bslink.vlan_tagging = True
    bslink.best_effort = True

# Request PC1
pc1 = request.RawPC("pc1")
if params.fixedid:
    pc1.component_id=params.fixedid
else:
    pc1.hardware_type = PCHWTYPE
pc1.disk_image = PCIMG

if params.intellibs:
    ilbspc1 = pc1.Blockstore( "intellibbs", params.intelmountpt )
    ilbspc1.dataset = params.INTEL_LIBS_URN
    ilbspc1.placement = "sysvol"

if params.matlabds:
    mlbs = pc1.Blockstore( "matlab", MATLAB_MP )
    mlbs.dataset = MATLAB_DS_URN
    mlbs.placement = "nonsysvol"

#Check to see if there are any interfaces attached to install the dhcp service
pc1.addService(pg.Execute(shell="sh", command="sudo chmod 775 /local/repository/faros_start.sh"))
pc1.addService(pg.Execute(shell="sh", command=STARTUP_SCRIPT))
if1pc = pc1.addInterface("if1pc", pg.IPv4Address("192.168.1.1", "255.255.255.0"))
#if1pc.bandwidth = 40 * 1000 * 1000 # 40 Gbps

bss1 = pc1.Blockstore("pc1scratch","/scratch")
bss1.size = "500GB"
bss1.placement = "nonsysvol"

# LAN connecting up everything (if needed).  Members are added below.
if len(params.mmimo_devices) or len(params.ue_devices):
    mmimolan = request.LAN("mmimolan")
    mmimolan.latency = 0
    mmimolan.vlan_tagging = False
    mmimolan.setNoBandwidthShaping()
    mmimolan.addInterface(if1pc)

    if len(params.mmimo_devices):
        # Request all Faros BSes requested
        for i, mmimodev in enumerate(params.mmimo_devices):
            mm = request.RawPC("mm%d" % i)
            mm.component_id = mmimodev.mmimoid
            mm.hardware_type = FAROSHWTYPE
            for j in range(params.hubints):
                mmif = mm.addInterface()
                mmimolan.addInterface(mmif)

    if len(params.ue_devices):
        for i, uedev in enumerate(params.ue_devices):
            ue = request.RawPC("ir%d" % i)
            ue.component_id = uedev.ueid
            ue.hardware_type = IRISHWTYPE
            ueif = ue.addInterface()
            mmimolan.addInterface(ueif)
    # Add frequency request(s)
    for frange in params.freq_ranges:
        request.requestSpectrum(frange.freq_min, frange.freq_max, 100)

# Print the RSpec to the enclosing page.
pc.printRequestRSpec()
